Table of Contents
=================
- [Table of Contents](#markdown-header-table-of-contents)
    - [Overview](#overview)  
      - [ 1. Gitflow ](#markdown-header-1-gitflow)  
      
      - [ 2. CI ](#markdown-header-2-ci)  
      
      - [ 3. CD ](#markdown-header-3-cd)  
      
      - [ 4. CI/CD in the Telio ](#markdown-header-4-cicd-in-the-telio)  
      
    - [ Details ](#markdown-header-details)  
    
      - [ 1. CodeBuild & CodePipeline ](#markdown-header-1-codebuild--codepipeline)  
      
        - [ a. Methodology ](#markdown-header-a-methodology)  
        
        - [ b. How to trigger CI/CD ](#markdown-header-b-how-to-trigger-cicd)  
        
      - [ 2. BitbucketCI & FluxCD ](#markdown-header-2-bitbucketci--fluxcd)  
      
        - [ a. BitbucketCI ](#markdown-header-a-bitbucketci)  
        
        - [ b. FluxCD ](#markdown-header-b-fluxcd)  
        
### Overview  
#### 1. Gitflow
Will update later

#### 2. CI
- Test, build & push artifact
  - Test
    - Unit test & Sonarqube for code smell, code coverage
  - Build & push artifact
    - Artifact can be docker image (for ECS/EKS services), S3 bucket (for frontend, serverless services)

#### 3. CD
- Deploy CI's artifact
- Telio have those environments match as branch name:
  - dev: Dev environment, for developer
  - stg: Staging environment, for QA
  - ppd: Pre product environment, same source code as staging but have number of data same as Production
  - hotfix/*: Hotfix environment, for hotfix
  - master: Production environment

#### 4. CI/CD in the Telio  
- *CodeBuild & CodePipeline*, apply for
  - Frontend services
  - Serverless services
  - ECS services: Will move all ECS services to EKS soon
- *BitbucketCI & FluxCD*, apply for
  - EKS services

### Details
#### 1. CodeBuild & CodePipeline
##### a. Methodology
- CodeBuild & CodePipeline provisioned by [this Terraform Repo](https://bitbucket.org/teliovn/telio-infra/src/master/). This repository contains:
  - CodeBuild & CodePipeline config
    - Production environment: Disable webhook for deploy manually
    - Other environments: Enable webhook for automated deploy
  - Service Git Repository URL
  - Branch name per environment
  - Domain name, certificate
  - And so on, not relate to the CI/CD section
- CodeBuild as CI stage:
  - Have two build projects:![CodeBuild](images/codebuild.png)
    - Contains *clone* keyword and Bitbucket source provider
      - Enable webhooks from CodeBuild to Bitbucket repo
      - When have new commit, trigger webhook and copy source code to S3 bucket as zip file
    - Contains *cp* keyword and CodePipeline source provider
      - Act as part of CodePipeline steps
- CodePipeline as CD stage:
  - Normally have three phases: ![CodePipeline](images/codepipeline.png)
    - Source: Listen from S3 artifact bucket on CI stage
    - Build:
      - Download & extract from Source
      - Do all of the steps define in the *buildspec.yml*, this file belong to Repository(follow this config) ![Buildspec](images/buildspec.png)
    - Deploy:
      - Deploy to target: S3 bucket for frontend, ECS ![ECS](images/ecs.png) ![FE](images/fe.png)

##### b. How to trigger CI/CD
- Non-Production environment:
  - Automatically trigger (because of webhook)
  - Have two ways when want to redeploy services:
    - Push dummy commit to trigger
    - AWS Console -> CodeBuild -> find services want to deploy with clone keyword -> *Start build* button  ![Trigger](images/trigger.png)
- Production environment:
  - Require manually deploy
    - AWS Console -> CodeBuild -> find services want to deploy with clone keyword -> *Start build* button![Trigger](images/trigger.png)

####  2. BitbucketCI & FluxCD
- Telio use GitOps approach (anti-pattern) for CI/CD, not follow CI/CD pipelines traditional way
  - CD is not depend on CI
  - BitbucketCI as CI stage
  - FluxCD as CD stage

##### a. BitbucketCI
- Prerequisite
  - Enable CI setting on Bitbucket repo ![EnableCI](images/enableCI.png)
- In the Git Repository of each service, will contains some CI part:
  ```
  .
  ├── Dockerfile # All of the steps require to build docker image
  ├── bitbucket-pipelines.yml # Define all of the CI steps
  ├── cicd # CI steps used by bitbucket-pipelines.yml
  │   ├── analysis.sh # Unit test, code smell, code coverage ...
  │   └── build.sh # build and push image to Image Registry(ECR)
  └── sonar-project.properties-template # Template to generate file use by Sonarqube
  ```
- bitbucket-pipelines.yml:
  - This file should be re-use and clear as much as possible, so all of the steps should be call through script file instead of write to this file.
  ```yaml
  definitions:
    services:
      docker: # Use docker dind to build
        memory: 2048
    steps:
      - step: &Analysis-and-test # Define step name
          name: analysis
          image:
              name: node:12 # Define kind when run the script
          caches:
            - node # Cache reduce significant build time, must have in any CI stuff
          script:
            - apt-get install bash -y
            - ./cicd/analysis.sh # Call to analysis.sh to test

      - step: &Build
          name: build
          services:
              - docker
          image:
            name: python:3.7.4-alpine3.10
          caches:
            - docker # Cache reduce significant build time, must have in any CI stuff
            - pip
          script:
            - apk add bash
            - ./cicd/build.sh # Call to build.sh to build & push image
          
  pipelines:
    default:
    - step: *Analysis-and-test # All other branch except branch in branches keyword will execute this step

    branches:
      "{dev,stg,ppd,master,hotfix/*}": # Define main branch to build & deploy
        - step: *Analysis-and-test
        - step: *Build
  ```
- analysis.sh
  - Run unit test
  - Connect to Sonarqube server to visualize code smell & code coverage
- build.sh
  - Contains rules to build and push docker images to correspond Image Registry
- View CI process & log ![processCI](images/processCI.png)
- All changes should begin from dev branch and merge to other branch follow by Git Flow

##### b. FluxCD
- [README.md](https://bitbucket.org/teliovn/k8s-tenant-cluster/src/master/README.md) in k8s-tenant-repo contains all informations about this section
  - How to Deploy/Update/Delete services
  - How to automate deplo on non-production environment